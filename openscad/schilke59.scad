defSchaftAussenUnten = 12; //orig 13
defSchaftAussenOben = 13; //orig 14
defSchaftHoehe = 54;
defRandBreite = 5;
defInnenDurchmesser = 30;
defRandHoehe = 10;
defKesselTiefe = 28;
defSchnauzerBreite = 60;
defSchnauzerHoehe = 20;
defSchnauzerStaerke = 5;
staerke = 3;
$fn=100;
module schaft(schaftAussenUnten=defSchaftAussenUnten, schaftAussenOben=defSchaftAussenOben, schaftHoehe=defSchaftHoehe) {
        cylinder(d1=schaftAussenUnten, d2=schaftAussenOben, h=schaftHoehe);
        
}

module kessel(randBreite=defRandBreite, innenDurchmesser=defInnenDurchmesser, randHoehe=defRandHoehe, kesselTiefe=defKesselTiefe) {
    
    
    translate([0,0,innenDurchmesser/2 + staerke]) difference() {
        union() {
            sphere(d=innenDurchmesser + 2 * staerke);
            
            translate([0,0,-0.1])
            cylinder(d=innenDurchmesser + 2 * staerke, h=kesselTiefe - innenDurchmesser / 2);
            translate([0,0,kesselTiefe - innenDurchmesser / 2 - randHoehe]) cylinder(d=innenDurchmesser + 2 * randBreite, h=randHoehe);
        }
    
        union() {
            sphere(d=innenDurchmesser);
            cylinder(d=innenDurchmesser, h=kesselTiefe * 2);
        }  
    }
}

module kern(schaftAussenUnten=defSchaftAussenUnten, schaftAussenOben=defSchaftAussenOben, schaftHoehe=defSchaftHoehe) {
    innerSizeBottom = schaftAussenUnten - 2;
    innerSizeTop = schaftAussenOben - 6;
    translate([0,0,-0.1]) cylinder(d1=innerSizeBottom, d2=innerSizeTop, h=schaftHoehe + staerke + 0.2);
}

module schnauzer(schnauzerBreite=defSchnauzerBreite, schnauzerHoehe=defSchnauzerHoehe, schnauzerStaerke=defSchnauzerStaerke) {
    translate([-schnauzerBreite/2,0,0])
    difference() {
        resize([schnauzerBreite, schnauzerHoehe, schnauzerStaerke])
        linear_extrude(height=1)
        import(file="schnauzer.dxf");
        
        rotate(a=[0,0,30])
        translate([9,-4,schnauzerStaerke-1])
        scale([0.9,0.9,1])
        linear_extrude(height=2)
        text("Richi", font="Beautiful ES");
    
        rotate(a=[0,0,-30])
        translate([schnauzerBreite/2 - 8,28,schnauzerStaerke-1])
        scale([0.7,0.7,1])
        linear_extrude(height=2)
        text("na muarz", font="Beautiful ES");
    }
}

module schilke(schaftAussenUnten=defSchaftAussenUnten, schaftAussenOben=defSchaftAussenOben, schaftHoehe=defSchaftHoehe, randBreite=defRandBreite, innenDurchmesser=defInnenDurchmesser, randHoehe=defRandHoehe, kesselTiefe=defKesselTiefe) {
    
    difference() {
        union() {
            schaft(schaftAussenUnten=schaftAussenUnten, schaftAussenOben=schaftAussenOben, schaftHoehe=schaftHoehe);
            translate([0,0,schaftHoehe - 3]) kessel(randBreite=randBreite, innenDurchmesser=innenDurchmesser, randHoehe=randHoehe, kesselTiefe=kesselTiefe);
            
            translate([0,innenDurchmesser / 2 - 3,schaftHoehe + kesselTiefe / 2])
            rotate(a=[0,180,0])
            schnauzer();
        }
        kern(schaftAussenUnten=schaftAussenUnten, schaftAussenOben=schaftAussenOben, schaftHoehe=schaftHoehe);
    }
}

rotate([180,0,0]) schilke(schaftAussenUnten=defSchaftAussenUnten, schaftAussenOben=defSchaftAussenOben, schaftHoehe=defSchaftHoehe, randBreite=defRandBreite + 2, innenDurchmesser=defInnenDurchmesser + 2, randHoehe=defRandHoehe, kesselTiefe=defKesselTiefe + 10);

//schilke();